# Display all IPv4 networks of Uruguay

This simple script get the latest base of IP from [lacnic.net](http://www.lacnic.net) and displays all IPv4 networks from Uruguay.

## Usage:
 
```
$ ./uy-ip-networks.sh

$ ./uy-ip-networks.sh 2>/dev/null    # Disable messages

$ ./uy-ip-networks.sh offline        # Use local file only
```

## Execution 

By default each script excecution checks the latest MD5 from lacnic.net (with curl) and compares with local database file. If local database do not exists or md5 check differs the database file is downloaded.

With _offilne_ parameter the script runs without any internet connection against the pre-installed local database file.


## License

```
The MIT License (MIT)
 
Copyright (c) 2015 Rodolfo Pilas <rodolfo@pilas.guru>
```

