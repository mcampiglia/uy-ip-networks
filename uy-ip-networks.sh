#!/bin/bash
#
# Checks and download latest delegated-lacnic IP list file
# Process all Uruguayan IPv4
# Print stdout IP/SUBNET
#
# Requires: curl

# The MIT License (MIT)
# 
# Copyright (c) 2015 Rodolfo Pilas <rodolfo@pilas.guru>
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

function log2 {
    local y=0
    for (( z=$1-1 ; $z > 0; z >>= 1 )) ; do
        let y=$y+1
    done
    echo $y
}

function processDB {		# Read delegated-lacnic-latest and output 
	for i in $(cat delegated-lacnic-latest | grep UY | grep ipv4 | cut -d\| -f 4,5); do
		IP=$(echo $i | cut -d\| -f1)
		HOSTS=$(echo $i | cut -d\| -f2)
		SUBNET=$(log2 $HOSTS)
		echo $IP/$SUBNET
	done
}

if [ "$#" -eq 0 ];then
 	# online process
	# Check & set environment
	type -P curl  &>/dev/null || { echo "This script requires 'curl' installed"; exit 1; }
	type -P md5  > /dev/null &&  CHKMD5="M"
	type -P md5sum > /dev/null && CHKMD5="L"

	LACNIC_LATEST="ftp://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-latest"
	LACNIC_MD5=$(curl -Bs ftp://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-latest.md5 | grep -oE '[^ ]+$')

	# Check & Download delegated-lacnic-latest if needed
	if [ ! -f delegated-lacnic-latest ]; then   			# if not exist
		echo "No local file, downloading delegated-lacnic-latest" >&2	# msg to stderr
        	curl -BsO $LACNIC_LATEST
	else  								# if local file exists
		if [ "$CHKMD5" == "M" ]; then 
			LOCAL_LATEST_MD5=$(md5 -q delegated-lacnic-latest)		   # MacOSX
		else
			LOCAL_LATEST_MD5=$(md5sum delegated-lacnic-latest | cut -d" " -f1) # Linux
		fi
		if [ ! $LOCAL_LATEST_MD5 == $LACNIC_MD5 ]; then   	# if local file is outdated
	       		echo "Update local delegated-lacnic-latest" >&2	# msg to stderr
	       		curl -BsO $LACNIC_LATEST
		fi
	fi
	processDB
fi
if [ "$1" == "offline" ]; then 
	if [ ! -f delegated-lacnic-latest ]; then                       # if not exist 
		echo "Local file database do not exist, offline execution not available" >&2
		exit
	else
		processDB
	fi
fi
exit 0

